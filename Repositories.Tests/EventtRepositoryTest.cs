using System;
using Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using NUnit.Framework;
using Repositories.PostgreSql;

namespace Repositories.Tests
{
    [TestFixture]
    public class EventtRepositoryTest
    {
        private IConfigurationRoot config;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            var env = Environment.GetEnvironmentVariable("DOTNET_ENVIRONMENT")?? "development";
            var configFile = 
            this.config = new ConfigurationBuilder()
                .AddJsonFile($"appsettings.{env}.json")
                .Build();
        }
        
        [Test]
        public void SaveAssignsId()
        {
            var connString = config.GetConnectionString("Default");
            var dataContext = new DataContext(connString);
            dataContext.Database.Migrate();
            var repo = new EventtRepository(dataContext);
            var eventt = Eventt.ForUser("juan", "gym", DateTime.Now.AddMinutes(1));
            
            repo.Save(eventt);
            
            Assert.That(eventt.Id, Is.GreaterThan(0));
        }
    }
}