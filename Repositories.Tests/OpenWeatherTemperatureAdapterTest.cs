using System;
using HttpClientFactoryLite;
using Microsoft.Extensions.Configuration;
using Moq;
using NUnit.Framework;

namespace Repositories.Tests
{
    public class OpenWeatherTemperatureAdapterTest
    {
        [Test]
        public void ReturnsTemperatureAsFloat()
        {
            var apiKey = Environment.GetEnvironmentVariable("OPENWEATHER_APIKEY");
            if (apiKey == null)
                Assert.Inconclusive("OPENWEATHER_APIKEY environment variable not set.");
            var configurationMockery = new Mock<IConfiguration>();
            configurationMockery.Setup(x => x["OpenWeatherApiKey"]).Returns(apiKey);
            var httpClientFactory = new HttpClientFactory();
            var adapter = new OpenWeatherTemperatureAdapter(configurationMockery.Object, httpClientFactory);
            adapter.Get();
            Assert.Pass();
        }
    }
}
