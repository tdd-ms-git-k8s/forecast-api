using System;
using System.Dynamic;
using System.Net.Http;
using System.Text.Json;
using Domain;
using Microsoft.Extensions.Configuration;
using HttpClientFactoryLite;

namespace Repositories
{
    public class OpenWeatherTemperatureAdapter : ITemperatureAdapter
    {
        private readonly string apiKey;
        private HttpClient httpClient;
        private const string QueryTemplate = "https://api.openweathermap.org/data/2.5/weather?q=London&appid=";

        public OpenWeatherTemperatureAdapter(IConfiguration config, HttpClientFactory httpClientFactory)
        {
            this.apiKey = config["OpenWeatherApiKey"];
            this.httpClient = httpClientFactory.CreateClient("c1");
        }

        public float Get()
        {
            var url = QueryTemplate + this.apiKey;
            var streamTask = this.httpClient.GetStreamAsync(url).Result;
            var rootElement = JsonDocument.Parse(streamTask).RootElement;
            dynamic openWeatherResponse = JsonSerializer.Deserialize<ExpandoObject>(rootElement.GetRawText());
            return Convert.ToSingle(openWeatherResponse.main.GetProperty("temp").GetDouble());
        }
    }
}
