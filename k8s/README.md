https://severalnines.com/database-blog/using-kubernetes-deploy-postgresql

$ kubectl get svc postgres
NAME       TYPE       CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE
postgres   NodePort   10.107.71.253   <none>        5432:31070/TCP   5m


$ psql -h localhost -U postgresadmin1 --password -p 31070 postgresdb
Password for user postgresadmin1: 
psql (10.4)
Type "help" for help.
  
postgresdb=#