using System;

namespace Domain
{
    public class Eventt
    {
        public Eventt()
        {
            // required for EF
        }
            
        private Eventt(string user, string title, in DateTime startDatetime)
        {
            this.Owner = user;
            this.Title = title;
            this.StartDateTime = startDatetime;
        }

        public int Id { get; set; }

        public DateTime StartDateTime { get; protected set; }

        public string Title { get; protected set; }

        public string Owner { get; protected set; }

        public static Eventt ForUser(string user, string title, in DateTime startDatetime)
        {
            return new Eventt(user, title, startDatetime);
        }
    }
}