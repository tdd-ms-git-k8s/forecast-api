namespace Domain
{
    public interface IDailyTaskRepository
    {
        void Save(DailyTask task);
    }
}