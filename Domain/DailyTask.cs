namespace Domain
{
    public class DailyTask
    {
        private readonly string title;

        public int Id { get; set; }

        public DailyTask(string title)
        {
            this.title = title;
        }

        public DailyTask()
        {
            
        }

        public static DailyTask WithTitle(string title)
        {
            return new DailyTask(title);
        }
        
    }
}