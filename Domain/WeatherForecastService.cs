using System.Threading.Tasks;

namespace Domain
{
    public class WeatherForecastService
    {
        private readonly ITemperatureAdapter temperatureAdapter;

        public WeatherForecastService(ITemperatureAdapter temperatureAdapter)
        {
            this.temperatureAdapter = temperatureAdapter;
        }

        public async Task<Forecast> Get()
        {
            return Forecast.Hot;
        }
    }
}
