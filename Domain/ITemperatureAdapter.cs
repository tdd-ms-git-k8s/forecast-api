﻿using System;
using System.Threading.Tasks;

namespace Domain
{
    public interface ITemperatureAdapter
    {
        float Get();
    }
}
