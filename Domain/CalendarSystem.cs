using System;
using System.Collections.Generic;

namespace Domain
{
    public class CalendarSystem
    {
        private IEventRepository eventRepository;

        public CalendarSystem(IEventRepository eventRepository)
        {
            this.eventRepository = eventRepository;
        }
        
        public virtual Eventt ScheduleEvent(string eventRequestUser, string eventRequestTitle, in DateTime eventRequestStartDateTime)
        {
            var eventt = Eventt.ForUser(eventRequestUser, eventRequestTitle, eventRequestStartDateTime);
            this.eventRepository.Save(eventt);
            return eventt;
        }

        public List<Eventt> ScheduleEventsFor(string user)
        {
            return this.eventRepository.FindByUser(user);
        }
    }
}