using System.Collections.Generic;

namespace Domain
{
    public interface IEventRepository
    {
        void Save(Eventt eventt);
        void DeleteAll();
        List<Eventt> FindByUser(string user);
    }
}