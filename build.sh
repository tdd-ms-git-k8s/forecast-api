#!/bin/bash

set -e

git show --oneline -s | cut -c 1-8 > WebApi/gitversion.txt
dotnet restore
dotnet build MicroservicesGitK8s.sln
