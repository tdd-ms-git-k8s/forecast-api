FROM mcr.microsoft.com/dotnet/sdk:3.1 AS build-env
WORKDIR /app
COPY . ./
RUN dotnet restore
WORKDIR /app/WebApi
RUN  dotnet publish -c Release -o out

FROM mcr.microsoft.com/dotnet/aspnet:3.1
LABEL author = "nicopaez@computer.org"
WORKDIR /app
COPY --from=build-env /app/WebApi/out .
ENTRYPOINT ["dotnet", "WebApi.dll"]
