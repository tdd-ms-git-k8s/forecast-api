using System.Collections.Generic;
using System.Linq;
using Domain;

namespace Repositories.PostgreSql
{
    public class EventtRepository : IEventRepository
    {
        private readonly DataContext dataContext;

        public EventtRepository(DataContext dataContext)
        {
            this.dataContext = dataContext;
        }

        public void Save(Eventt eventt)
        {
            this.dataContext.Add(eventt);
            this.dataContext.SaveChanges();
        }

        public void DeleteAll()
        {
            this.dataContext.Eventts.RemoveRange(this.dataContext.Eventts);
            this.dataContext.SaveChanges();
        }

        public List<Eventt> FindByUser(string user)
        {
            return this.dataContext.Eventts.ToList();
        }
    }
}