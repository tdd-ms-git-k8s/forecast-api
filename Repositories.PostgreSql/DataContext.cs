using System;
using Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Repositories.PostgreSql
{
    public class DataContext : DbContext
    {
        private string connString = "name=ConnectionStrings:Default";

        public DataContext()
        {
            this.connString = "host=localhost;port=5432;database=postgres;user id=postgres;password=mysecret";
        }

        public DataContext(string connectionString)
        {
            this.connString = connectionString;
        }

        public DataContext(DbContextOptions<DataContext> options): base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            => optionsBuilder.UseNpgsql(this.connString, options => options.SetPostgresVersion(new Version(9, 6)));

        public DbSet<DailyTask> DailyTasks { get; set; }

        public DbSet<Eventt> Eventts { get; set; }
    }
}
