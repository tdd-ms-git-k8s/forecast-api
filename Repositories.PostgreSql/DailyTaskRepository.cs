﻿using Domain;

namespace Repositories.PostgreSql
{
    public class DailyTaskRepository : IDailyTaskRepository
    {
        private readonly DataContext dataContext;

        public DailyTaskRepository(DataContext dataContext)
        {
            this.dataContext = dataContext;
        }
        public void Save(DailyTask dailyTask)
        {
            this.dataContext.Add(dailyTask);
            this.dataContext.SaveChanges();
        }
    }
}
