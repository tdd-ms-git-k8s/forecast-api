using System.Threading.Tasks;
using Moq;
using NUnit.Framework;

namespace Domain.Tests
{
    public class WeatherForecastServiceTest
    {
        [Test]
        [Category("unit")]
        public async Task ForecastIsHotWhenTemperatureOver25()
        {
            var temperatureAdapterMockery = new Mock<ITemperatureAdapter>();
            temperatureAdapterMockery.Setup(x => x.Get()).Returns(30.0f);
            var forecastService = new WeatherForecastService(temperatureAdapterMockery.Object);

            var forecast = await forecastService.Get();

            Assert.That(forecast, Is.EqualTo(Forecast.Hot));
        }
    }
}
