using System;
using Moq;
using NUnit.Framework;

namespace Domain.Tests
{
    [TestFixture]
    public class CalendarTest
    {
        [Test]
        public void CreateSimpleEventWhenCalendarIsEmpty()
        {
            var eventRepo = Mock.Of<IEventRepository>();
            var calendar = new CalendarSystem(eventRepo);
            var scheduledEvent = calendar.ScheduleEvent("someuser@somemail.com", "some event", DateTime.Now.AddHours(1));
            Assert.IsNotNull(scheduledEvent);
        }
    }
}