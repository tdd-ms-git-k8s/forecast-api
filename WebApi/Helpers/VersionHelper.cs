using System.IO;

namespace WebApi.Helpers
{
    public class VersionHelper
    {
        public static string GetVersion()
        {
            var assembly = typeof(WebApi.Startup).Assembly;
            var versionNumber = assembly.GetName().Version?.ToString();
            using var reader = new StreamReader(assembly.GetManifestResourceStream("WebApi.gitversion.txt"));
            var versionHash = reader.ReadToEnd().Trim();
            return $"{versionNumber}-{versionHash}";
        }
    }
}
