using System;

namespace WebApi.Requests
{
    public class EventRequest
    {
        public string User { get; set; }

        public string Title { get; set; }
        
        public DateTime StartDateTime { get; set; }
    }
}