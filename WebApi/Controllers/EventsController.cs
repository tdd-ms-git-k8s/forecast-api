using System.ComponentModel.DataAnnotations;
using System.Net;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApi.Requests;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EventsController : ControllerBase
    {
        public EventsController(CalendarSystem calendar)
        {
            this.CalendarSystem = calendar;
        }

        [HttpGet]
        public ActionResult Index(string user)
        {
            var scheduledEvents =
                this.CalendarSystem.ScheduleEventsFor(user);
            return new ObjectResult(scheduledEvents)
            {
                StatusCode = (int?) HttpStatusCode.OK,
            };
        }

        [HttpPost]
        public ActionResult Create([Required, FromBody] EventRequest eventRequest)
        {
            var scheduledEvent =
                this.CalendarSystem.ScheduleEvent(eventRequest.User, eventRequest.Title, eventRequest.StartDateTime);
            return new ObjectResult(scheduledEvent)
            {
                StatusCode = (int?) HttpStatusCode.Created,
            };
        }

        public CalendarSystem CalendarSystem { get; set; }
    }
}