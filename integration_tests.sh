#!/bin/bash

set -e

function run_test {
  project=$1
  options=$2
  dotnet test "${project}/${project}.csproj" --filter Category!=unit --test-adapter-path:. --logger:"nunit;LogFilePath=../test-results/${project}-inte-results.xml" /p:CollectCoverage=true /p:CoverletOutput=../coverage.info /p:MergeWith=../coverage.info /p:ExcludeByFile="**/Reference.cs" $options
}

git show --oneline -s | cut -c 1-8 > WebApi/gitversion.txt
dotnet restore
dotnet build MicroservicesGitK8s.sln

run_test Domain.Tests
run_test Repositories.Tests
run_test WebApi.Tests /p:CoverletOutputFormat=opencover

