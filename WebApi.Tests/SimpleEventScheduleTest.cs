using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using Repositories.PostgreSql;
using WebApi.Tests.Support;

namespace WebApi.Tests
{
    [TestFixture]
    public class SimpleEventScheduleTest
    {
        private TestWebApplicationFactory<Startup> factory;
        private HttpClient client;

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            this.factory = new TestWebApplicationFactory<Startup>();
            this.client = this.factory.CreateClient();

            using (var testScope = this.factory.Services.CreateScope())
            {
                var dataContext = testScope.ServiceProvider.GetService<DataContext>();
                dataContext.Database.Migrate();
            }
        }

        [SetUp]
        public void SetUp()
        {
            using (var testScope = this.factory.Services.CreateScope())
            {
                var repo = testScope.ServiceProvider.GetService<IEventRepository>();
                repo.DeleteAll();
            }
        }

        [Test]
        public void SuccessfulScheduling()
        {
            var johnGymSession = new
            {
                user = "john",
                title = "gym session",
                start_datetime = DateTime.Now.AddHours(1),
            };
            var httpContent = new StringContent(JsonSerializer.Serialize(johnGymSession));
            httpContent.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");

            var result = this.client.PostAsync("/events", httpContent).Result;

            Assert.That(result.StatusCode, Is.EqualTo(HttpStatusCode.Created));
            var stringResponse = result.Content.ReadAsStringAsync().Result;
            var options = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true,
            };
            var createdEvent = JsonSerializer.Deserialize<Eventt>(stringResponse, options);
            Assert.That(createdEvent.Id, Is.GreaterThan(0));
        }

        [Test]
        public void QueryScheduledEvents()
        {
            var johnGymSession = new
            {
                user = "john",
                title = "gym session",
                start_datetime = DateTime.Now.AddHours(1),
            };
            var httpContent = new StringContent(JsonSerializer.Serialize(johnGymSession));
            httpContent.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");

            var result = this.client.PostAsync("/events", httpContent).Result;

            Assert.That(result.StatusCode, Is.EqualTo(HttpStatusCode.Created));

            result = this.client.GetAsync("/events?user=jhon").Result;
            
            Assert.That(result.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            
            var stringResponse = result.Content.ReadAsStringAsync().Result;
            var options = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true,
            };
            var schedulesEventts = JsonSerializer.Deserialize<List<Eventt>>(stringResponse, options);
            Assert.That(schedulesEventts.Count, Is.EqualTo(1));
        }
    }
}