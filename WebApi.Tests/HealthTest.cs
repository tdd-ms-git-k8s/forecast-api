using System.Dynamic;
using System.Net;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using NUnit.Framework;
using WebApi.Tests.Support;

namespace WebApi.Tests
{
    public class HealthTest
    {
        private TestWebApplicationFactory<Startup> factory;
        private HttpClient client;

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            this.factory = new TestWebApplicationFactory<Startup>();
            this.client = this.factory.CreateClient();
        }

        [Test]
        public async Task IncludesVersion()
        {
            var result = await this.client.GetAsync("/health");
            Assert.That(result.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            dynamic jsonResult = JsonSerializer.Deserialize<ExpandoObject>(result.Content.ReadAsStringAsync().Result);
            var version = typeof(WebApi.Startup).Assembly.GetName().Version;
            if (version != null)
            {
                var expectedVersion = version.ToString();
                Assert.That(jsonResult.version.ToString(), Is.SupersetOf(expectedVersion));
            }
        }
    }
}
